terraform {
  backend "s3" {
    bucket = "fr.alltech.terraform"
    key    = "kubernetes/state"
    region = "eu-west-3"
  }
}