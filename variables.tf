variable "kubernetes_client_certificate"{
  type = "string"
}

variable "kubernetes_cluster_ca_certificate"{
  type = "string"
}

variable "kubernetes_client_key"{
  type = "string"
}

variable "host"{
  type = "string"
}
