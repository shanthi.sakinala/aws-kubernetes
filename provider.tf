# Provider
provider "kubernetes" {
  host                   = var.host
  client_certificate     = base64decode(var.kubernetes_client_certificate)
  client_key             = base64decode(var.kubernetes_client_key)
  cluster_ca_certificate = base64decode(var.kubernetes_cluster_ca_certificate)
  load_config_file = false
}

provider "aws" {
  version    = "~> 2.0"
  region     = var.ec2_region
}
