variable "mysql-password" {}
variable "mysql-img" {}
variable "mysql-dir" {}
variable "mysql-lbl" {}
variable "web-port" {}
variable "web-img" {}
variable "web-host" {}
variable "web-dir" {}
variable "web-lbl" {}


resource "kubernetes_persistent_volume" "pv-mysql" {
  metadata {
    name = "pv-mysql"
  }
  spec {
    capacity = {
      storage = "1Gi"
    }
    access_modes = ["ReadWriteMany"]
    persistent_volume_source {
      host_path {
        path = var.mysql-dir
      }
    }
  }
}

//resource "kubernetes_persistent_volume" "pv-web" {
//  metadata {
//    name = "pv-web"
//  }
//  spec {
//    capacity = {
//      storage = "1Gi"
//    }
//    access_modes = ["ReadWriteMany"]
//    persistent_volume_source {
//      host_path {
//        path = var.web-dir
//      }
//    }
//  }
//}

resource "kubernetes_persistent_volume_claim" "pvc-mysql" {
  metadata {
    name = "pvc-mysql"
  }
  spec {
    access_modes = ["ReadWriteMany"]
    resources {
      requests = {
        storage = "1Gi"
      }
    }
    volume_name = element(kubernetes_persistent_volume.pv-mysql.metadata.*.name, 0)
  }
}

//resource "kubernetes_persistent_volume_claim" "pvc-web" {
//  metadata {
//    name = "pvc-web"
//  }
//  spec {
//    access_modes = ["ReadWriteMany"]
//    resources {
//      requests = {
//        storage = "1Gi"
//      }
//    }
//    volume_name = element(kubernetes_persistent_volume.pv-web.metadata.*.name, 0)
//  }
//}

resource "kubernetes_secret" "mysql" {
  metadata {
    name = "mysql-pass"
  }
  data = {
    password = var.mysql-password
  }
}


resource "kubernetes_deployment" "dep-mysql" {
  metadata {
    name = "dep-mysql"
    labels = {
      app = var.mysql-lbl
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.mysql-lbl
      }
    }

    template {
      metadata {
        labels = {
          app = var.mysql-lbl
        }
      }

      spec {
        container {
          image = var.mysql-img
          name  = "wp-mysql"
          env {
            name = "MYSQL_ROOT_PASSWORD"
            value_from {
              secret_key_ref {
                name = element(kubernetes_secret.mysql.metadata.*.name, 0)
                key = "password"
              }
            }
          }
          port {
            container_port = 3306
            name = "mysql"
          }
          volume_mount {
            name = "mysql-persistent-storage"
            mount_path = "/var/lib/mysql"
          }
        }
        volume {
          name = "mysql-persistent-storage"
          persistent_volume_claim {
            claim_name = element(kubernetes_persistent_volume_claim.pvc-mysql.metadata.*.name, 0)
          }
        }
      }
    }
  }
}

resource "kubernetes_service" "svc-mysql" {
  metadata {
    name = "svc-mysql"
  }
  spec {
    selector = {
      app = var.mysql-lbl
    }
    port {
      port = 3306
    }
    cluster_ip = "None"
  }
}


resource "kubernetes_deployment" "dep-web" {
  metadata {
    name = "dep-web-php"
    labels = {
      app = var.web-lbl
    }
  }

  spec {
    replicas = 1

    selector {
      match_labels = {
        app = var.web-lbl
      }
    }

    template {
      metadata {
        labels = {
          app = var.web-lbl
        }
      }

      spec {
        container {
          image = var.web-img
          name  = "web-php"
          env {
            name = "DB_HOST"
            value = "svc-mysql"
          }

          env {
            name = "DB_PASSWORD"
            value_from {
              secret_key_ref {
                name = element(kubernetes_secret.mysql.metadata.*.name, 0)
                key = "password"
              }
            }
          }

          port {
            container_port = var.web-port
            name = "web-php"
          }
//          volume_mount {
//            name = "wordpress-persistent-storage"
//            mount_path = "/var/www/html"
//          }
        }
//        volume {
//          name = "wordpress-persistent-storage"
//          persistent_volume_claim {
//            claim_name =  element(kubernetes_persistent_volume_claim.pvc-web.metadata.*.name, 0)
//          }
//        }

      }
    }
  }
}

resource "kubernetes_service" "svc-web" {
  metadata {
    name = "svc-web"
  }
  spec {
    selector = {
      app = var.web-lbl
    }
    port {
      port = var.web-port
    }

    type = "NodePort"
  }
}

resource "kubernetes_ingress" "web-ingress" {
  metadata {
    name = "web-ingress"
    annotations = {
      "nginx.ingress.kubernetes.io/rewrite-target" = "/"
    }
  }

  spec {
    rule {
      host = var.web-host
      http {
        path {
          backend {
            service_name = element(kubernetes_service.svc-web.metadata.*.name, 0)
            service_port = var.web-port
          }
          path = "/"
        }
      }
    }
  }
}
